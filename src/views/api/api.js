import axios from 'axios'

// 查询用户 登录
export const getAccount = params => {
  return axios({
    method: 'post',
    url: '/api/login/getAccount',
    dataType: 'json',
    data: params
  })
}
// 查询所有用户
export const getAllUser = params => {
  return axios({
    method: 'post',
    url: '/api/index/getAllUser',
    dataType: 'json',
    data: params
  })
}
// 写入修改用户信息
export const setUser = params => {
  return axios({
    method: 'post',
    url: '/api/user/setUser',
    dataType: 'json',
    data: params
  })
}
// 根据id,删除用户 传一个数组
export const removeUser = params => {
  return axios({
    method: 'post',
    url: '/api/user/removeUser',
    dataType: 'json',
    data: params
  })
}
// 创建公告
export const createNotice = params => {
  return axios({
    method: 'post',
    url: '/api/notice/createNotice',
    dataType: 'json',
    data: params
  })
}
// 获取公告
export const getNotice = params => {
  return axios({
    method: 'post',
    url: '/api/notice/getNotice',
    dataType: 'json',
    data: params
  })
}
// 保存编辑的公告信息
export const setNotice = params => {
  return axios({
    method: 'post',
    url: '/api/notice/setNotice',
    dataType: 'json',
    data: params
  })
}
// 根据id,删除公告 传一个数组
export const removeNotice = params => {
  return axios({
    method: 'post',
    url: '/api/notice/removeNotice',
    dataType: 'json',
    data: params
  })
}
// 查询帖子
export const getPost = params => {
  return axios({
    method: 'post',
    url: '/api/post/getPost',
    dataType: 'json',
    data: params
  })
}
// 获取所有模块信息
export const getModule = params => {
  return axios({
    method: 'post',
    url: '/api/module/getModule',
    dataType: 'json',
    data: params
  })
}
// 写入修改帖子信息
export const setPost = params => {
  return axios({
    method: 'post',
    url: '/api/post/setPost',
    dataType: 'json',
    data: params
  })
}
// 新增模块
export const createModule = params => {
  return axios({
    method: 'post',
    url: '/api/module/createModule',
    dataType: 'json',
    data: params
  })
}
// 获取模块
export const getAllModule = params => {
  return axios({
    method: 'post',
    url: '/api/module/getAllModule',
    dataType: 'json',
    data: params
  })
}
// 保存状态审核
export const setModule = params => {
  return axios({
    method: 'post',
    url: '/api/module/setModule',
    dataType: 'json',
    data: params
  })
}
// 保存显示状态
export const setDisplayModule = params => {
  return axios({
    method: 'post',
    url: '/api/module/setDisplayModule',
    dataType: 'json',
    data: params
  })
}
// 根据id,删除帖子 传一个数组
export const removePost = params => {
  return axios({
    method: 'post',
    url: '/api/Post/removePost',
    dataType: 'json',
    data: params
  })
}
// 根据id,删除模块 传一个数组
export const removeModule = params => {
  return axios({
    method: 'post',
    url: '/api/module/removeModule',
    dataType: 'json',
    data: params
  })
}
// 删除评论
export const deletePostcomment = params => {
  return axios({
    method: 'post',
    url: '/api/post/deletePostcomment',
    dataType: 'json',
    data: params
  })
}