// Schema、Model、Entity或者Documents的关系请牢记，Schema生成Model，Model创造Entity，Model和Entity都可对数据库操作造成影响，但Model比Entity更具操作性。
// Mongoose就是一个让我们可以通过Node来操作MongoDB的模块
const mongoose = require('mongoose')
// 连接数据库 如果不自己创建 默认test数据库会自动生成
mongoose.connect('mongodb://localhost/test')

// 为这次连接绑定事件
const db = mongoose.connection
db.once('error', () => console.log('Mongo connection error'))
db.once('open', () => console.log('Mongo connection successed'))

/** ************ 1 定义模式loginSchema **************/
const loginSchema = mongoose.Schema({
  username: String,
  password: String
})
// 个人用户信息表
const userSchema = mongoose.Schema({
  username: String, // 用户名 建议使用学号
  password: {
    type: String,
    set (val) {
      // 通过bcryptjs对密码加密返回值 第一个值返回值， 第二个密码强度
      return require('bcryptjs').hashSync(val, 10)
    }
  }, // 密码
  phone: Number, // 手机号码
  sex: String, // 性别
  college: String, // 学院
  headPic: Object, // 头像
  email: String, // 邮箱
  safetyProblem1: String, // 安全问题1
  safetyProblem2: String, // 安全问题2
  safetyAnswer1: String, // 安全问题1
  safetyAnswer2: String, // 安全问题2
  role: Number, // 角色 0管理员 2老师 1学生 3超级管理员
  applyTime: Date, // 申请时间
  grade: Number, // 等级
  status: Number, // 审核状态 1通过 0未通过
  reportNum: Number, // 被举报次数
  star: Array, // 我的收藏
  lastSignInTime: Date // 最后登录时间 每次登录成功就写进数据库
})

// 板块信息表 模块消息表
const moduleSchema = mongoose.Schema({
  modulename: String, // 模块名称 唯一性
  status: Number, // 审核状态 0未审核 1审核通过 2审核不通过 管理员审核通过后才显示在上面
  createTime: Date, // 创建时间
  display: Number, // 是否显示改板块 1显示 0不显示 默认显示
  postID: Array, // 帖子id 存属于这个板块所有的帖子id
  userID: String, // 用户ID 创建此模块的用户ID
  applyReason: String, // 申请原因
  noPassReason: String // 审核不通过原因
})

// 帖子信息表
const postSchema = mongoose.Schema({
  title: String, // 帖子标题 不唯一
  content: String, // 帖子内容
  pictures: Array, // 图片
  status: Number, // 审核状态 0未审核 1审核通过 2审核不通过
  user: Array, // 用户信息 创建此模块的用户信息
  createTime: Date, // 创建时间
  display: Number, // 是否显示该帖子 1显示 2不显示 默认显示
  readNum: Number, // 阅读次数 访问次数，不是浏览次数  前端每调用成功一次页面数据接口，就相当于阅读了一次文章，前端获取到数据成功后，将访问量字段的值自增1个，然后将增加后访问量字段再传给后端，保存在数据库中）
  commentList: Array, // 评论列表 每条数据里面有用户数据，评论时间、评论内容、点赞列表
  modulename: String, // 模块名称
  reportList: Array, // 举报次数 存用户id 一个人只能举报一次
  likeList: Array // 点赞次数 存用户id
})

// 回帖信息表
const replySchema = mongoose.Schema({
  postID: String, // 帖子ID 被回复的帖子ID
  userID: String, // 回复人ID
  content: String, // 回帖内容
  replyTime: Date, // 回复时间
  likeNum: Number, // 点赞次数
  floorNum: Number // 排序  楼数 查找这个帖子ID的回帖消息，如果长度为0的话则排序为1，1的话则为2
})
// 公告信息表
const noticeSchema = mongoose.Schema({
  title: String, // 公告标题
  content: String, // 公告内容
  createTime: Date, // 创建时间
  readNum: Number // 阅读次数
})

/** ************ 2 定义模型Model **************/
// 通过Schema来创建Model
// Model代表的是数据库中的集合，通过Model才能对数据库进行操作
// mongoose.model(modelName,schema) (集合名，Schema)
// modelName 就是要映射的集合名，mongoose会自动将集合名变成复数
const Models = {
  Login: mongoose.model('Login', loginSchema),
  User: mongoose.model('User', userSchema),
  Module: mongoose.model('Module', moduleSchema),
  Post: mongoose.model('Post', postSchema),
  Reply: mongoose.model('Reply', replySchema),
  Notice: mongoose.model('Notice', noticeSchema)
}

module.exports = Models
