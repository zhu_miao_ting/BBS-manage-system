const models = require('./db')
const express = require('express')
const fs = require('fs')
const router = express.Router()
/** ************ 创建(create) 读取(get) 更新(update) 删除(delete) **************/

// 获取已有账号接口 登录功能
router.post('/api/login/getAccount', (req, res) => {
  // 通过模型去查找数据库
  let username = req.body.username
  let password = req.body.password
  models.Login.find({
    username: username
  }, (err, data) => {
    if (err) {
      res.send(err)
    }
    if (data.length === 0) {
      res.send({
        'code': -1,
        'msg': '用户不存在',
        'Info': data
      })
    } else if (data.length > 0) {
      if (password !== data[0].password) {
        res.send({
          'code': -1,
          'msg': '密码不正确',
          'Info': data
        })
      } else {
        res.send({
          'code': 0,
          'msg': '登录成功',
          'Info': data
        })
      }
    }
  })
})
// 获取所有用户信息
router.post('/api/index/getAllUser', (req, res) => {
  // 通过模型去查找数据库
  let pageSize = req.body.pageSize
  let pageIndex = req.body.pageIndex
  if (req.body.role) {
    if (req.body.role === '学生') {
      req.body.role = 1
    } else {
      req.body.role = 2
    }
  }
  if (req.body.username && req.body.role) {
    models.User.find({
      "username": {
        $regex: req.body.username
      },
      role: req.body.role
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的用户',
          'Info': data
        })
      } else if (data.length > 0) {
        models.User.find({
          "username": {
            $regex: req.body.username
          },
          role: req.body.role
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  } else if (req.body.username && req.body.role === '') {
    models.User.find({
      "username": {
        $regex: req.body.username
      }
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的用户',
          'Info': data
        })
      } else if (data.length > 0) {
        models.User.find({
          "username": {
            $regex: req.body.username
          }
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  } else if (!req.body.username && req.body.role) {
    models.User.find({
      role: req.body.role
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的用户',
          'Info': data
        })
      } else if (data.length > 0) {
        models.User.find({
          role: req.body.role
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  } else {
    models.User.find({}, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有存在用户',
          'Info': data
        })
      } else if (data.length > 0) {
        models.User.find({}, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }
})
// 写入修改用户信息
router.post('/api/user/setUser', (req, res) => {
  let whereStr = {
    _id: req.body.id
  }
  let updateStr = {
    $set: {
      'password': req.body.password, // 密码
      'phone': req.body.phone, // 手机号码
      'college': req.body.college, // 学院
      'role': req.body.role, // 角色
      'sex': req.body.sex === '1' ? 1 : 2, // 性别
      'email': req.body.email, // 邮箱
      'safetyProblem1': req.body.safetyProblem1, // 安全问题1
      'safetyAnswer1': req.body.safetyAnswer1, // 安全问题答案1
      'safetyProblem2': req.body.safetyProblem2, // 安全问题2
      'safetyAnswer2': req.body.safetyAnswer2, // 安全问题答案2
    }
  }
  models.User.updateOne(whereStr, updateStr, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      models.User.find({
        _id: req.body.id
      }, (err, datas) => {
        if (err) {
          res.send(err)
        } else {
          res.send({
            'code': 0,
            'msg': '编辑成功',
            'data': datas
          })
        }
      })
    }
  })
})
// 根据id,删除用户 传一个数组
router.post('/api/user/removeUser', (req, res) => {
  models.User.remove({
    _id: {
      $in: req.body.multipleSelection
    }
  }, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      res.send({
        'code': 0,
        'msg': '删除成功',
        'Info': data
      })
    }
  })
})
// 创建公告
router.post('/api/notice/createNotice', (req, res) => {
  let newNotice = new models.Notice({
    title: req.body.title, // 公告标题
    content: req.body.content, // 公告内容
    createTime: new Date(), // 创建时间
    readNum: 0 // 阅读次数 访问次数，不是浏览次数  前端每调用成功一次页面数据接口，就相当于阅读了一次文章，前端获取到数据成功后，将访问量字段的值自增1个，然后将增加后访问量字段再传给后端，保存在数据库中）
  })
  // 保存数据,数据写进mongoDB数据库
  newNotice.save((err, data) => {
    if (err) {
      res.send(err)
    } else {
      res.send({
        'code': 0,
        'msg': '发布成功',
        'data': data
      })
    }
  })
})
// 获取公告
router.post('/api/notice/getNotice', (req, res) => {
  // 通过模型去查找数据库
  let pageSize = req.body.pageSize
  let pageIndex = req.body.pageIndex
  if (req.body.title) {
    // 模糊搜索
    models.Notice.find({
      "title": {
        $regex: req.body.title
      }
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的公告',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Notice.find({
          "title": {
            $regex: req.body.title
          }
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  } else {
    models.Notice.find({}, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有存在用户',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Notice.find({}, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }
})
// 保存编辑的公告信息
router.post('/api/notice/setNotice', (req, res) => {
  let whereStr = {
    _id: req.body.id
  }
  let updateStr = {
    $set: {
      'title': req.body.title, // 标题
      'content': req.body.content, // 内容
    }
  }
  models.Notice.updateOne(whereStr, updateStr, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      models.Notice.find({
        _id: req.body.id
      }, (err, datas) => {
        if (err) {
          res.send(err)
        } else {
          res.send({
            'code': 0,
            'msg': '编辑成功',
            'data': datas
          })
        }
      })
    }
  })
})
// 根据id,删除公告 传一个数组
router.post('/api/notice/removeNotice', (req, res) => {
  models.Notice.remove({
    _id: {
      $in: req.body.multipleSelection
    }
  }, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      res.send({
        'code': 0,
        'msg': '删除成功',
        'Info': data
      })
    }
  })
})
// 获取帖子
router.post('/api/post/getPost', (req, res) => {
  // 通过模型去查找数据库
  let pageSize = req.body.pageSize
  let pageIndex = req.body.pageIndex
  if (req.body.title&&!req.body.display) {
    // 模糊搜索
    models.Post.find({
      "title": {
        $regex: req.body.title
      }
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的帖子',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Post.find({
          "title": {
            $regex: req.body.title
          }
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }else if(!req.body.title&&req.body.display){
    models.Post.find({
      display: req.body.display
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的帖子',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Post.find({
          display: req.body.display
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  } else if(req.body.title&&req.body.display){
    models.Post.find({
      "title": {
        $regex: req.body.title
      },
      display: req.body.display
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的帖子',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Post.find({
          "title": {
            $regex: req.body.title
          },
          display: req.body.display
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }else {
    models.Post.find({}, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有存在帖子',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Post.find({
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }
})
// 获取所有模块信息
router.post('/api/module/getModule', (req, res) => {
  // 通过模型去查找数据库
  // let modulename = req.body.modulename
  models.Module.find({
    // modulename: modulename
  }, (err, data) => {
    if (err) {
      res.send(err)
    }
    if (data.length === 0) {
      res.send({
        'code': -1,
        'msg': '不存在任何模块',
        'Info': data
      })
    } else if (data.length > 0) {
      res.send({
        'code': 0,
        'msg': '查询成功',
        'Info': data
      })
    }
  })
})
// 写入修改帖子信息
router.post('/api/post/setPost', (req, res) => {
  let whereStr = {
    _id: req.body.id
  }
  let updateStr = {
    $set: {
      'title': req.body.title, // 标题
      'content': req.body.content, // 内容
      'display': req.body.display, // 显示状态
      'readNum': req.body.readNum, // 浏览次数
      'modulename': req.body.modulename // 所属模块
    }
  }
  models.Post.updateOne(whereStr, updateStr, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      models.Post.find({
        _id: req.body.id
      }, (err, datas) => {
        if (err) {
          res.send(err)
        } else {
          res.send({
            'code': 0,
            'msg': '编辑成功',
            'data': datas
          })
        }
      })
    }
  })
})
// 新增模块
router.post('/api/module/createModule', (req, res) => {
  // 通过模型去查找数据库
  let modulename = req.body.modulename
  models.Module.find({
    modulename: modulename
  }, (err, data) => {
    if (err) {
      res.send(err, req)
    }
    if (data.length > 0) {
      res.send({
        'code': 1,
        'msg': '该模块已存在，请创建别的模块。',
        'Info': data
      })
    } else if (data.length === 0) {
      let newModule = new models.Module({
        modulename: req.body.modulename, // 模块名称
        status: 1, // 审核状态 管理员审核通过后才显示在上面 0未审核 1审核通过 2审核不通过
        createTime: new Date(), // 创建时间
        display: 1, // 是否显示 1显示 0不显示
        postID: [], // 帖子id
        userID: '管理员创建', // 用户id
        applyReason: '管理员创建', // 申请原因
        noPassReason: '' // 审核不通过原因
      })
      // 保存数据,数据写进mongoDB数据库
      newModule.save((err, data) => {
        if (err) {
          res.send(err)
        } else {
          res.send({
            'code': 0,
            'msg': '模块创建成功',
            'data': data
          })
        }
      })
    }
  })
})
// 获取模块
router.post('/api/module/getAllModule', (req, res) => {
  // 通过模型去查找数据库
  let pageSize = req.body.pageSize
  let pageIndex = req.body.pageIndex
  if(req.body.status===0){
    req.body.status=3
  }
  if (req.body.modulename&&!req.body.status) {
    // 模糊搜索
    models.Module.find({
      "modulename": {
        $regex: req.body.modulename
      }
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的模块',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Module.find({
          "modulename": {
            $regex: req.body.modulename
          }
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }else if(req.body.modulename===''&&req.body.status){
    if(req.body.status===3){
      req.body.status=0
    }
    models.Module.find({
      status: req.body.status
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的模块1',
          'Info': data
        })
      } else if (data.length > 0) {
        if(req.body.status===3){
          req.body.status=0
        }
        models.Module.find({
          status: req.body.status
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  } else if(req.body.modulename&&req.body.status){
    if(req.body.status===3){
      req.body.status=0
    }
    models.Module.find({
      "modulename": {
        $regex: req.body.modulename
      },
      status: req.body.status
    }, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有符合条件的模块2',
          'Info': data
        })
      } else if (data.length > 0) {
        if(req.body.status===3){
          req.body.status=0
        }
        models.Module.find({
          "modulename": {
            $regex: req.body.modulename
          },
          status: req.body.status
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }else {
    models.Module.find({}, (err, data) => {
      if (err) {
        res.send(err)
      }
      if (data.length === 0) {
        res.send({
          'code': -1,
          'msg': '没有存在模块',
          'Info': data
        })
      } else if (data.length > 0) {
        models.Module.find({
        }, (err, datas) => {
          if (err) {
            res.send(err)
          }
          res.send({
            'code': 0,
            'msg': '查询成功',
            'Info': data,
            'total': datas.length
          })
        })
      }
    }).skip(pageSize * (pageIndex - 1)).limit(pageSize)
  }
})
// 保存状态审核
router.post('/api/module/setModule', (req, res) => {
  let whereStr = {
    _id: req.body.id
  }
  let updateStr = {
    $set: {
      'status': req.body.status,
      'noPassReason': req.body.noPassReason,
    }
  }
  models.Module.updateOne(whereStr, updateStr, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      models.Module.find({
        _id: req.body.id
      }, (err, datas) => {
        if (err) {
          res.send(err)
        } else {
          res.send({
            'code': 0,
            'msg': '成功',
            'data': datas
          })
        }
      })
    }
  })
})
// 保存显示状态
router.post('/api/module/setDisplayModule', (req, res) => {
  let whereStr = {
    _id: req.body.id
  }
  let updateStr = {
    $set: {
      'display': req.body.display
    }
  }
  models.Module.updateOne(whereStr, updateStr, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      models.Module.find({
        _id: req.body.id
      }, (err, datas) => {
        if (err) {
          res.send(err)
        } else {
          res.send({
            'code': 0,
            'msg': '成功',
            'data': datas
          })
        }
      })
    }
  })
})
// 根据id,删除帖子 传一个数组
router.post('/api/Post/removePost', (req, res) => {
  models.Post.remove({
    _id: {
      $in: req.body.multipleSelection
    }
  }, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      res.send({
        'code': 0,
        'msg': '删除成功',
        'Info': data
      })
    }
  })
})
// 根据id,删除模块 传一个数组
router.post('/api/module/removeModule', (req, res) => {
  models.Module.remove({
    _id: {
      $in: req.body.multipleSelection
    }
  }, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      res.send({
        'code': 0,
        'msg': '删除成功',
        'Info': data
      })
    }
  })
})
// 删除评论
router.post('/api/post/deletePostcomment', (req, res) => {
  // 通过模型去查找数据库
  // let index = req.body.index
  models.Post.updateOne({
    _id: req.body.id,
    'commentList.createTime': req.body.createTime
  }, {
    $set: {
      'commentList.$.content': ''
    }
  }, (err, data) => {
    if (err) {
      res.send(err)
    }
    if (data.length === 0) {
      res.send({
        'code': -1,
        'msg': '网络错误',
        'Info': data
      })
    } else {
      res.send({
        'code': 0,
        'msg': '删除成功',
        'Info': data
      })
    }
  })
})
module.exports = router